﻿CREATE TABLE [Contact].[usr]
(
	[id] INT NOT NULL PRIMARY KEY identity,
	[name] varchar(50),
	[tel] varchar(50),
	[relation] varchar(50),
	[sex] varchar(50),
	_time datetime default getUtcDate(),
	_memo nvarchar(max),
)
