﻿CREATE TABLE [nilnul._acc.usr].[token]
(
    [id] bigINT NOT NULL PRIMARY KEY identity
    ,
    [usr] bigint references [nilnul].[Use](id)
    ,
    [token] UniqueIDENTIFIER default newid()
    ,
    time datetime default getlUtcDate()
    ,
    memo nvarchar(max)
    