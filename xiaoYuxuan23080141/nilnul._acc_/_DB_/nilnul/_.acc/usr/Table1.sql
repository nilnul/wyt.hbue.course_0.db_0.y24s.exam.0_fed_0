﻿CREATE TABLE [nilnul].[Usr]
(
	[id] INT NOT NULL PRIMARY KEY identity
	,
	[name] nvarchar(400), 
    [pass.tip] NVARCHAR(400) NULL
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)
)
