﻿CREATE TABLE [dbo].[duzhe]
(
	[ReaderId] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(100) NULL, 
    [PhoneNumber] NVARCHAR(20) NULL, 
    [Address] NVARCHAR(MAX) NULL
)
