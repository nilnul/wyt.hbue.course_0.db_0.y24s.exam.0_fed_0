﻿CREATE TABLE [dbo].[Table1]
(
	[BookId] INT NOT NULL PRIMARY KEY, 
    [Title] NVARCHAR(225) NULL, 
    [Author] NVARCHAR(100) NULL, 
    [ISBE] NVARCHAR(20) NULL, 
    [Publisher] NVARCHAR(100) NULL, 
    [PublicationDate] DATE NULL
)
