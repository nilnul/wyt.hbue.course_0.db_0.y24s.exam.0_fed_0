﻿CREATE TABLE [nilnul._acc.usr].[Token]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[usr] bigint references [nilnul._acc].[Usr](id)
	, 
    [token] UniqueIDENTIFIER default newid()
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)

)
