﻿CREATE TABLE Albums (  
    AlbumID INT PRIMARY KEY IDENTITY(1,1),  
    Title NVARCHAR(255) NOT NULL,  
    ReleaseDate DATE NULL,  
    ArtistID INT,  
    FOREIGN KEY (ArtistID) REFERENCES Artists(ArtistID)  
); 