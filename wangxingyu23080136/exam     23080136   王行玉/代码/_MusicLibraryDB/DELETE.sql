﻿USE MusicLibraryDB;  
GO  
  

DECLARE @ArtistName NVARCHAR(255) = 'Justin Bieber';  
  
  
DELETE FROM Tracks  
WHERE AlbumID IN (  
    SELECT AlbumID FROM Albums  
    WHERE ArtistID IN (  
        SELECT ArtistID FROM Artists  
        WHERE Name = @ArtistName  
    )  
);  
  
  
DELETE FROM Albums  
WHERE ArtistID IN (  
    SELECT ArtistID FROM Artists  
    WHERE Name = @ArtistName  
);  
  
  
DELETE FROM Artists  
WHERE Name = @ArtistName;  
  
DECLARE @AlbumTitle NVARCHAR(255) = 'Purpose';  
  
  
DELETE FROM Tracks  
WHERE AlbumID IN (  
    SELECT AlbumID FROM Albums  
    WHERE Title = @AlbumTitle  
);  
  
  
DELETE FROM Albums  
WHERE Title = @AlbumTitle;  
  
  
  
GO