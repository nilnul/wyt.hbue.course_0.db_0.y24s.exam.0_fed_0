﻿USE MusicLibraryDB;  
GO  
  

SELECT ArtistID, Name, Description  
FROM Artists;  
  

SELECT Albums.AlbumID, Albums.Title, Artists.Name AS ArtistName  
FROM Albums  
JOIN Artists ON Albums.ArtistID = Artists.ArtistID  
WHERE Albums.Title LIKE '%Love%';  
  
 
DECLARE @AlbumTitle NVARCHAR(255) = 'Purpose';  
  
SELECT Tracks.TrackID, Tracks.Title, Tracks.Duration  
FROM Tracks  
JOIN Albums ON Tracks.AlbumID = Albums.AlbumID  
WHERE Albums.Title = @AlbumTitle;  
  

DECLARE @ArtistName NVARCHAR(255) = 'Justin Bieber'; '  
  
SELECT   
    Artists.Name AS ArtistName,  
    Albums.AlbumID,  
    Albums.Title AS AlbumTitle,  
    Tracks.TrackID,  
    Tracks.Title AS TrackTitle,  
    Tracks.Duration  
FROM   
    Artists  
JOIN   
    Albums ON Artists.ArtistID = Albums.ArtistID  
LEFT JOIN   
    Tracks ON Albums.AlbumID = Tracks.AlbumID  
WHERE   
    Artists.Name = @ArtistName;  
  
GO