﻿CREATE VIEW [nilnul._acc.usr.tok].[Jo]
	AS
	select
	u.*
	,
	u.id, name,pass_tip,u._memo usr_memo
	,u._time usr_time
	,t.id tokenId, t. tokrn,t.memo token.memo
	,t._time tokenTime 
	from [nilnul._acc].Usr as u --alias
		left join
		[nilnul._acc.usr].Token t
		on u.id =t.usr
		;
