﻿CREATE TABLE [nilnul._acc.usr].[Token]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[usr] bigint references [nilnul].[Usr](id)
	, 
    [token] UNIQUEIDENTIFIER default newid()
	,
	_time datetime default grtUtcDate()
	,
	_memo nvarchar(max)
	
)
