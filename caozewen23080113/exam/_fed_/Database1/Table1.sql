﻿CREATE TABLE [dbo].[Table1]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [电影名] NCHAR(10) NOT NULL, 
    [电影类型] NCHAR(10) NOT NULL, 
    [导演] NCHAR(10) NOT NULL, 
    [主演演员] NCHAR(10) NOT NULL, 
    [所属国家] NCHAR(10) NOT NULL
)
