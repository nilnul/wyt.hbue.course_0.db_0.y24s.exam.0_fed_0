﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 记账本
{
    public partial class ForgetPwd : Form
    {
        public ForgetPwd()
        {
            InitializeComponent();
            newAgainPwdTextBox.PasswordChar = '*';
            newPwdTextBox.PasswordChar = '*';
         
        }
        //修改按钮
        private void modifyButton_Click(object sender, EventArgs e)
        {
            var userphoneID = IDTextBox.Text;//获取身份证号码
            var newPwd = newPwdTextBox.Text;//获取修改的新密码
            var newAgainPwd = newAgainPwdTextBox.Text;//获取重复修改的新密码

            if (userphoneID != "" && newPwd != "" && newAgainPwd != "")
            {
                if (userphoneID.Length != 11)
                {
                    MessageBox.Show("手机号不足18位!");
                }
                else if (newPwd != newAgainPwd)
                {
                    MessageBox.Show("两次输入的密码不一致!");
                }
                else
                {
                    SqlConnection connection = new SqlConnection();
                    connection.ConnectionString = PublicMethod._connectionString;
                    connection.Open();
                    string mysql = "select * from users where phone='" + userphoneID + "'";//定义查询语句 

                    SqlCommand sqlCom = new SqlCommand(mysql, connection);
                    SqlDataReader reader = sqlCom.ExecuteReader();
                    if (reader.Read())
                    {

                        if (reader["phone"].ToString().Trim() != userphoneID)
                        {
                            MessageBox.Show("输入的手机号不准确!");
                        }
                        else
                        {
                            reader.Close();
                            string updateSql = "update users set pas='" + newPwd + "'where phone='" + userphoneID + "'";
                            SqlCommand updateCom = new SqlCommand(updateSql, connection);
                            updateCom.ExecuteNonQuery();

                            if (MessageBox.Show("密码修改成功！", "提示", MessageBoxButtons.OK) == DialogResult.OK)
                            {
                                reader.Close();
                                connection.Close();
                                this.Close();//关闭窗口


                            }
                        }

                    }
                    else
                    {

                        MessageBox.Show("修改失败！");
                    }
                }
            }
            else
            {
                MessageBox.Show("输入框不能为空!");
            }
        }
        //返回按钮
        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
