﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 记账本
{
    public partial class UserHome : Form
    {
        public UserHome()
        {
            InitializeComponent();
           

        }

    
        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 form = new Form1();
            form.ShowDialog();
            Application.Exit();
        }
       
        private void 记账ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminJz adminJz = new AdminJz();
            adminJz.ShowDialog();
            Application.Exit();
        }

        private void 注销ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定注销此用户吗！", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                SqlConnection connection = new SqlConnection(PublicMethod._connectionString);
                connection.Open();
              
                string deleteSql = "delete from users where name='" + PublicMethod.user_name + "'";
                SqlCommand deleteCom = new SqlCommand(deleteSql, connection);
                deleteCom.ExecuteNonQuery();
                MessageBox.Show("注销成功！");
                connection.Close();
                this.Hide();
                Form1 form = new Form1();
                form.ShowDialog();
                Application.Exit();
            }
        }
    }
}
