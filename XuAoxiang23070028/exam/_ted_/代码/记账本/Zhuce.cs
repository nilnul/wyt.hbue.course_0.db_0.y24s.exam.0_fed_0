﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 记账本
{
    public partial class Zhuce : Form
    {
        public Zhuce()
        {
            InitializeComponent();
           
        }
        //返回按钮
        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            var username = usernameTextBox.Text;//获取注册的用户名
            var password = passwordTextBox4.Text;//获取注册的用户密码
            var againPassword = pwdAgainiTextBox5.Text;//获取注册的重复用户密码
            var phone = textBox2.Text;
              
            if (password != "" && username != "" && againPassword != "" && phone != "" )
            {

                if (password != againPassword)
                {
                    MessageBox.Show("两次密码不一样!");
                }
               
                else if (phone.Length != 11)
                {
                    MessageBox.Show("电话号码必须为11位!");
                }
                else
                {
                    SqlConnection connection = new SqlConnection(PublicMethod._connectionString);
                    connection.Open();
                    string findIDSql = "select phone from users where phone='" + phone + "'";//判断手机号是否存在
                    SqlCommand sqlCom = new SqlCommand(findIDSql, connection);
                    SqlDataReader sdr = sqlCom.ExecuteReader();
                    sdr.Read();
                    if (sdr.HasRows)
                    {
                        MessageBox.Show("该手机号已经存在!");
                    }
                    else
                    {
                        PublicMethod query = new PublicMethod();
                        string strSql = "insert into users(name,phone,pas) values ('" +
                              username + "','" + phone + "','" + password + "')";
                        if (query.ExecuteNonQuery(strSql, PublicMethod._connectionString) > 0)
                        {
                            if (MessageBox.Show("注册成功！确定返回登录", "提示", MessageBoxButtons.OK) == DialogResult.OK)
                            {
                                this.Close();//关闭窗口
                            }
                        }
                        else
                        {
                            MessageBox.Show("注册失败！", "信息提示");
                        }
                    }
                    sdr.Close();
                    connection.Close();
                }
            }
        }
    }
}
