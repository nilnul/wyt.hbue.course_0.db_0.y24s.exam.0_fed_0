﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 记账本
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            passwordTextbox.PasswordChar = '*';
        }
        //登录按钮
        private void longinButton_Click(object sender, EventArgs e)
        {
            var username = usernameTextbox.Text;//获取用户名
            var password = passwordTextbox.Text;//获取密码

            if (username != "" || password != "")
            {

                    PublicMethod.user_name = username;
                    string str = "select * from users where name='" + username + "'and pas='" + password + "'";
                    login(str);
               

            }
            else
            {
                MessageBox.Show("用户名或者密码不能为空!");
            }
        }
        
        //登录方法
        private void login(string str)
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                connection.ConnectionString = PublicMethod._connectionString;
                connection.Open();
                string findUserSql = str;//定义查询语句
                SqlCommand sqlCom = new SqlCommand(findUserSql, connection);
                SqlDataReader sdr = sqlCom.ExecuteReader();
                sdr.Read();                                  
                    if (sdr.HasRows)
                    {                    
                        this.Hide();
                        UserHome form = new UserHome();
                        form.ShowDialog();
                        Application.Exit();

                    }
                    else
                    {
                        MessageBox.Show("登录失败！");
                    }              
            }

            finally
            {
                connection.Close();
            }


        }
      

        private void 退出系统ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }
        //注册用户
        private void 用户注册ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Zhuce form = new Zhuce();
            form.ShowDialog();
          
        }
        //忘记密码
        private void ModifyPwdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            ForgetPwd form = new ForgetPwd();
            form.ShowDialog();
           
        }


      
    }
}
