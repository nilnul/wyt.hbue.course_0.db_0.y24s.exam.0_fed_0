﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace 记账本
{
    public partial class AddOrUpdateJz : Form
    {
        public string ward_update;//公共变量用来保存修改信息时的id值
        public AddOrUpdateJz()
        {
            InitializeComponent();
            if (PublicMethod.test_flags == "1")
            {
                this.Text = "添加账本信息";
            }
            if (PublicMethod.test_flags == "2")
            {
                this.Text = "修改账本信息";
            }
          
        }
        
 
        //确定按钮
        private void button1_Click(object sender, EventArgs e)
        {
            //当添加信息时
            if (PublicMethod.test_flags == "1")
            {
                string zffs = textBox1.Text;//支付方式
                string zc = textBox2.Text;//支出
                string sr = textBox3.Text;//收入
                string time = dateTimePicker1.Text;//时间
                
              

                if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && dateTimePicker1.Text != "")
                {

                    SqlConnection connection = new SqlConnection(PublicMethod._connectionString);
                    connection.Open();
                    string strSql = "insert into zbmessage(zffs,zc,sr,time) values(@zffs,@zc,@sr,@time)";
                    SqlCommand sqlCom = new SqlCommand(strSql, connection);
                    sqlCom.Parameters.AddWithValue("@zffs", zffs);
                    sqlCom.Parameters.AddWithValue("@zc", zc);
                    sqlCom.Parameters.AddWithValue("@sr", sr);
                    sqlCom.Parameters.AddWithValue("@time", time);
                    sqlCom.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("添加成功!");
                    AdminJz add = new AdminJz();
                    this.Hide();
                    add.ShowDialog();
                    Application.Exit();
                }
                else
                {
                    MessageBox.Show("输入框不能为空!");
                }
            }
            //修改信息操作时
            if (PublicMethod.test_flags == "2")
            {
                string zffs = textBox1.Text;//支付方式
                string zc = textBox2.Text;//支出
                string sr = textBox3.Text;//收入
                string time = dateTimePicker1.Text;//时间

                if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && dateTimePicker1.Text != "")
                {

                    SqlConnection connection = new SqlConnection(PublicMethod._connectionString);
                    connection.Open();


                    string strSql = "update zbmessage set zffs=@zffs,zc=@zc,sr=@sr,time=@time where id='" + ward_update + "'";
                    SqlCommand sqlCom = new SqlCommand(strSql, connection);
                    sqlCom.Parameters.AddWithValue("@zffs", zffs);
                    sqlCom.Parameters.AddWithValue("@zc", zc);
                    sqlCom.Parameters.AddWithValue("@sr", sr);
                    sqlCom.Parameters.AddWithValue("@time", time);
                    sqlCom.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("修改成功!");
                    AdminJz add = new AdminJz();
                    this.Hide();
                    add.ShowDialog();
                    Application.Exit();
                }

                else
                {
                    MessageBox.Show("输入框不能为空!");
                }
            }

        }
        //返回按钮
        private void button2_Click(object sender, EventArgs e)
        {
            AdminJz add = new AdminJz();
            this.Hide();
            add.ShowDialog();
            Application.Exit();
        }
    }
}
