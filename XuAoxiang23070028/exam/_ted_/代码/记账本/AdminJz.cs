﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 记账本
{
    public partial class AdminJz : Form
    {
        public AdminJz()
        {
            InitializeComponent();
            //不显示空白新增行
            dataGridView1.AllowUserToAddRows = false;
            // 隐藏DataGridView的行标题（第一列空白列）
            dataGridView1.RowHeadersVisible = false;
            getTest();
        }
       
        //获取信息方法
        private void getTest()
        {
            SqlConnection sqlConnection = new SqlConnection(PublicMethod._connectionString);
            sqlConnection.Open();
            string cmdText = "select * from zbmessage";
            SqlCommand selectCommand = new SqlCommand(cmdText, sqlConnection);
            SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
            mySqlDataAdapter.SelectCommand = selectCommand;
            DataSet dataSet = new DataSet();
            mySqlDataAdapter.Fill(dataSet, "zbmessage");//将筛选出来的数据放到表中
            // myTicketInfo.DataSource = null;
            dataGridView1.DataSource = dataSet.Tables["zbmessage"];
            sqlConnection.Close();
            dataGridView1.Columns["时间"].DisplayIndex = 3;

        }
        //返回按钮
        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserHome form = new UserHome();
            form.ShowDialog();
            Application.Exit();
        }
        //重置按钮
        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            getTest();
        }
        //查询按钮
        private void button1_Click(object sender, EventArgs e)
        {

            var name = textBox1.Text;//获取输入的支付方式或者时间
            SqlConnection sqlConnection = new SqlConnection(PublicMethod._connectionString);
            sqlConnection.Open();
            string cmdText = "select * from zbmessage where zffs='" + name + "' and time = '" + dateTimePicker1.Text + "'";
            SqlCommand selectCommand = new SqlCommand(cmdText, sqlConnection);
            SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter();
            mySqlDataAdapter.SelectCommand = selectCommand;
            DataSet dataSet = new DataSet();
            mySqlDataAdapter.Fill(dataSet, "zbmessage");
            // myTicketInfo.DataSource = null;
            dataGridView1.DataSource = dataSet.Tables["zbmessage"];
            sqlConnection.Close();
            dataGridView1.Columns["时间"].DisplayIndex = 3;

        }
      
        //添加病房信息
        private void button2_Click(object sender, EventArgs e)
        {
            PublicMethod.test_flags = "1";
            this.Hide();
            AddOrUpdateJz form = new AddOrUpdateJz();
            form.ShowDialog();
            Application.Exit();
        }
        //对表中的数据进行操作
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "修改")
            {
                if (MessageBox.Show("确定修改此消息吗！", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                 
                    PublicMethod.test_flags = "2";
                    AddOrUpdateJz up = new AddOrUpdateJz();
                    up.ward_update = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();//当前id
                    up.textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();//支付方式
                    up.textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();//收入
                    up.textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();//支出
                    up.dateTimePicker1.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();//时间
                    this.Hide();
                    up.ShowDialog();//显示信息修改界面
                    Application.Exit();
                    getTest();

                }

            }
            if (dataGridView1.Columns[e.ColumnIndex].Name == "删除")
            {
                if (MessageBox.Show("确定删除此消息吗！", "提示", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    SqlConnection connection = new SqlConnection(PublicMethod._connectionString);
                    connection.Open();
                    string id = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();//当前信息id
                    string deleteSql = "delete from zbmessage where id='" + id + "'";
                    SqlCommand deleteCom = new SqlCommand(deleteSql, connection);
                    deleteCom.ExecuteNonQuery();
                    MessageBox.Show("删除成功！");
                    connection.Close();
                    getTest();
                }

            }
        }

        private void AdminWard_Load(object sender, EventArgs e)
        {
            //主页面显示数据
                getTest();          
        }
    }
}
