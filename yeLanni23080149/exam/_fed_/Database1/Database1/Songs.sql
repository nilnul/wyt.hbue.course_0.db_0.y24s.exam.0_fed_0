﻿CREATE TABLE [dbo].[Songs] (  
    SongId INT PRIMARY KEY IDENTITY(1,1),  
    Title NVARCHAR(100) NOT NULL,  
    Duration TIME,  
    AlbumId INT,  
    FOREIGN KEY (AlbumId) REFERENCES Albums(AlbumId)  
);