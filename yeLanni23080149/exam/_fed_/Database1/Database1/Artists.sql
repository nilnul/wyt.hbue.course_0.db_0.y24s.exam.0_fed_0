﻿CREATE TABLE [dbo].[Artists] (  
    ArtistId INT PRIMARY KEY IDENTITY(1,1),  
    Name NVARCHAR(100) NOT NULL,  
    Description NVARCHAR(MAX)  
);