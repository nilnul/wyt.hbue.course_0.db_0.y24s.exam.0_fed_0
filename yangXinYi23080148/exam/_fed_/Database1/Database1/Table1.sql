﻿CREATE TABLE [dbo].[Table1]
(
	[MovieID] INT NOT NULL PRIMARY KEY, 
    [Title] NCHAR(255) NOT NULL, 
    [Genre] NCHAR(100) NULL, 
    [Director] NCHAR(255) NULL, 
    [ReleaseYear ] INT NULL
)
