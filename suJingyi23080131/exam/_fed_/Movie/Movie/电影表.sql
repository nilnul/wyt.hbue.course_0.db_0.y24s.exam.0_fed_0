﻿CREATE TABLE [dbo].[电影表]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [title] NVARCHAR(255) NOT NULL, 
    [release_year] DATETIME NOT NULL, 
    [genre] NCHAR(10) NOT NULL, 
    [director_id] NCHAR(10) NOT NULL, 
    [actor_id] NCHAR(10) NOT NULL, 
    [user_id] NCHAR(10) NOT NULL, 
    [comment_id] NCHAR(10) NOT NULL
)
