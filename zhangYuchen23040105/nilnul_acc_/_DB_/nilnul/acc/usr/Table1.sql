﻿CREATE TABLE [nilnul].[Usr]
(
	[Id] bigINT NOT NULL PRIMARY KEY identity
	,
	[name] nvarchar(400),
	[pass_tip] NVARCHAR(400) NULL
	,
	_time datetime default getUtcDate()
	,
)
