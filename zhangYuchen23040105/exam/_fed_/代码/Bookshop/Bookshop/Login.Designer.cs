﻿namespace Bookshop
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Upasswd = new System.Windows.Forms.TextBox();
            this.Uname = new System.Windows.Forms.TextBox();
            this.Ulogin = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.llogin = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Window;
            this.label7.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label7.Location = new System.Drawing.Point(1020, -2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 46);
            this.label7.TabIndex = 3;
            this.label7.Text = "×";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Thistle;
            this.label8.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label8.Location = new System.Drawing.Point(469, 334);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 46);
            this.label8.TabIndex = 3;
            this.label8.Text = "用户名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Thistle;
            this.label2.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label2.Location = new System.Drawing.Point(469, 409);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 46);
            this.label2.TabIndex = 3;
            this.label2.Text = "密     码";
            // 
            // Upasswd
            // 
            this.Upasswd.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Upasswd.Location = new System.Drawing.Point(609, 406);
            this.Upasswd.Name = "Upasswd";
            this.Upasswd.Size = new System.Drawing.Size(236, 54);
            this.Upasswd.TabIndex = 2;
            // 
            // Uname
            // 
            this.Uname.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Uname.Location = new System.Drawing.Point(609, 335);
            this.Uname.Name = "Uname";
            this.Uname.Size = new System.Drawing.Size(236, 54);
            this.Uname.TabIndex = 2;
            // 
            // Ulogin
            // 
            this.Ulogin.AutoSize = true;
            this.Ulogin.BackColor = System.Drawing.Color.Thistle;
            this.Ulogin.Font = new System.Drawing.Font("思源黑体 Medium", 7.5F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Ulogin.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.Ulogin.Location = new System.Drawing.Point(604, 536);
            this.Ulogin.Name = "Ulogin";
            this.Ulogin.Size = new System.Drawing.Size(118, 29);
            this.Ulogin.TabIndex = 1;
            this.Ulogin.Text = "管理员登陆";
            this.Ulogin.Click += new System.EventHandler(this.Ulogin_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label4.Location = new System.Drawing.Point(363, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(251, 46);
            this.label4.TabIndex = 1;
            this.label4.Text = "云书店管理系统";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(229, 303);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 46);
            this.label1.TabIndex = 3;
            this.label1.Text = "品质保证";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Thistle;
            this.label5.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label5.Location = new System.Drawing.Point(229, 371);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 46);
            this.label5.TabIndex = 3;
            this.label5.Text = "快速送到";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Thistle;
            this.label6.Font = new System.Drawing.Font("思源黑体 Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label6.Location = new System.Drawing.Point(229, 444);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 46);
            this.label6.TabIndex = 3;
            this.label6.Text = "放心购物";
            // 
            // llogin
            // 
            this.llogin.BackColor = System.Drawing.Color.Thistle;
            this.llogin.FlatAppearance.BorderColor = System.Drawing.Color.LavenderBlush;
            this.llogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.llogin.Location = new System.Drawing.Point(587, 486);
            this.llogin.Name = "llogin";
            this.llogin.Size = new System.Drawing.Size(147, 47);
            this.llogin.TabIndex = 6;
            this.llogin.Text = "登     录";
            this.llogin.UseVisualStyleBackColor = false;
            this.llogin.Click += new System.EventHandler(this.llogin_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Thistle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1, 90);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1072, 604);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(1073, 783);
            this.Controls.Add(this.llogin);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Uname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Upasswd);
            this.Controls.Add(this.Ulogin);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "  ";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Upasswd;
        private System.Windows.Forms.TextBox Uname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Ulogin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button llogin;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}