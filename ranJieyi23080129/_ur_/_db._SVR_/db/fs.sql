﻿create database my3
    on primary(
        name=my1_data
       ,
       filename=N'c:\dbSrv\my3_data.mdf'
   )
      log on (
          name=my1_log
          ,
          filename=N'c:\dbSrv\my3_log.ldf'
   )
 ;