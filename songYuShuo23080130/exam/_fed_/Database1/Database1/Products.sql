﻿CREATE TABLE  [nilnul]
(  
    ProductID INT PRIMARY KEY,  
    ProductName VARCHAR(255) NOT NULL,  
    QuantityInStock INT NOT NULL DEFAULT 0,  
    ReorderLevel INT,  
    UnitPrice DECIMAL(10, 2) NOT NULL DEFAULT 0.00  
);