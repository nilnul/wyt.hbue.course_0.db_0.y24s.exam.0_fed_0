﻿CREATE TABLE Suppliers (  
    SupplierID INT PRIMARY KEY,  
    SupplierName VARCHAR(255) NOT NULL,  
    ContactName VARCHAR(255),  
    PhoneNumber VARCHAR(20),  
    Email VARCHAR(255),  
    Address VARCHAR(255),  
    City VARCHAR(100),  
    State VARCHAR(100),  
    Country VARCHAR(100)  
);