﻿CREATE TABLE [dbo].[Table1]
(  
    WarehouseID INT PRIMARY KEY,  
    WarehouseName VARCHAR(255) NOT NULL,  
    Address VARCHAR(255),  
    City VARCHAR(100),  
    State VARCHAR(100),  
    Country VARCHAR(100)  
);