﻿-- To allow advanced options to be changed.
EXECUTE sp_configure 'show advanced options', 1;


GO
/*batch*/

-- To update the currently configured value for advanced options.
RECONFIGURE;
GO

-- To enable the feature.
EXECUTE sp_configure 'xp_cmdshell', 1;
GO

-- To update the currently configured value for this feature.
RECONFIGURE;
GO


--for proxy?
--GRANT exec ON xp_cmdshell TO N'<some_user>';

exec xp_cmdShell 'md c:\a'
; --output in table;
go


restore database my -- you can change the name
	from
	disk=N'C:\db24s_1\Weide23080137\_ur_\db.bak_\my.bak'
	with
		move N'my1_data' to N'C:\a\t1.mdf'
		,
		move N'my1_log' to N'C:\a\t1.ldf'
	--also you can rename files		
;
go
-- To disable the feature.
EXECUTE sp_configure 'xp_cmdshell', 0;
GO

-- To update the currently configured value for this feature.
RECONFIGURE;



-- To set "show advanced options" back to false
EXECUTE sp_configure 'show advanced options', 0;
GO

-- To update the currently configured value for advanced options.
RECONFIGURE;
GO