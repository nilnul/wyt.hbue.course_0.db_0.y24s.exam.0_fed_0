﻿create database my2 
	on primary (
		name=my1_data
		,
		fileName= N'c:\dbSrv\my2_data.mdf'
	)
	log on (
		name=my1_log
		,
		fileName=N'c:\dbSrv\my2_log.ldf'
	)
;