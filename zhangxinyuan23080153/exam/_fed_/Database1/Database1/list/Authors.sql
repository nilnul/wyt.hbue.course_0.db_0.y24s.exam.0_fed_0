﻿CREATE TABLE [nilnul._acc]
(
	[author-id] INT NOT NULL PRIMARY KEY, 
    [first-name] VARCHAR(50) NULL, 
    [last-name] VARCHAR(50) NULL
)
