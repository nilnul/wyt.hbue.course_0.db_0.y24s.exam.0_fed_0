﻿CREATE TABLE [nilnul]
(
	[book-id] INT NOT NULL PRIMARY KEY, 
    [title] NVARCHAR(MAX) NULL, 
    [author-id] INT NULL, 
    [category-id] INT NULL, 
    [isbn] NVARCHAR(MAX) NULL, 
    [publication] DATE NULL
)
