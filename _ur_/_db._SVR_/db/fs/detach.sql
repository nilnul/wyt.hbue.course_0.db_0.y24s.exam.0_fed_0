﻿/*
 detach the db from the svr, and as a consequence, the files are detached altogether with the db from the server
*/

sp_detach_db  --stored procedure
	N'my2'
	, 
	'true' --skipChecks
;
