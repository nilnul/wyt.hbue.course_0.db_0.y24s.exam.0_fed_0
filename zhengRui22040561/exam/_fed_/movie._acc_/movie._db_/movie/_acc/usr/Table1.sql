﻿CREATE TABLE [dbo].[Table1]
(
	[Id] INT NOT NULL PRIMARY KEY
	,
	[movie.name] nvarchar(400)
	, 
    [score] NVARCHAR(400) NULL 
	,
	[people] NVARCHAR(400) NULL
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)
)
