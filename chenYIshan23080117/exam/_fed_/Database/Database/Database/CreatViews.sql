﻿CREATE VIEW UserOrderCounts AS
SELECT 
    u.UserId,
    u.Username,
    COUNT(o.OrderId) AS TotalOrders
FROM 
    Users u
LEFT JOIN 
    Orders o ON u.UserId = o.UserId
GROUP BY 
    u.UserId, u.Username;