﻿CREATE proc/*edure*/ [nilnul.acc.carte_.rsc.proc_].[Keyword]
	@keyword as nvarchar(4000)
	,
	@rows as int output
AS 

select
	top 1000 
		*
		from 
			[nilnul.acc.carte_].Rsc
	where 
		uri like stuff(N'%%',2,0, @keyword) 
	;

set @rows = @@rowcount 

return 0;
	
