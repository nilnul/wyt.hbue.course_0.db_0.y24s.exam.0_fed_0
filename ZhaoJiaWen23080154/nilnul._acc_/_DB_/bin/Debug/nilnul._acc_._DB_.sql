﻿/*
nilnul._acc_._DB_ 的部署脚本

此代码由工具生成。
如果重新生成此代码，则对此文件的更改可能导致
不正确的行为并将丢失。
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "nilnul._acc_._DB_"
:setvar DefaultFilePrefix "nilnul._acc_._DB_"
:setvar DefaultDataPath "C:\Users\L4M402N054\AppData\Local\Microsoft\VisualStudio\SSDT\nilnul._acc_"
:setvar DefaultLogPath "C:\Users\L4M402N054\AppData\Local\Microsoft\VisualStudio\SSDT\nilnul._acc_"

GO
:on error exit
GO
/*
请检测 SQLCMD 模式，如果不支持 SQLCMD 模式，请禁用脚本执行。
要在启用 SQLCMD 模式后重新启用脚本，请执行:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'要成功执行此脚本，必须启用 SQLCMD 模式。';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                CURSOR_DEFAULT LOCAL 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET PAGE_VERIFY NONE,
                DISABLE_BROKER 
            WITH ROLLBACK IMMEDIATE;
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET TARGET_RECOVERY_TIME = 0 SECONDS 
    WITH ROLLBACK IMMEDIATE;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE (CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367)) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
PRINT N'正在创建 架构 [nilnul]...';


GO
CREATE SCHEMA [nilnul]
    AUTHORIZATION [dbo];


GO
PRINT N'正在创建 架构 [nilnul._acc]...';


GO
CREATE SCHEMA [nilnul._acc]
    AUTHORIZATION [dbo];


GO
PRINT N'正在创建 架构 [nilnul._acc.usr]...';


GO
CREATE SCHEMA [nilnul._acc.usr]
    AUTHORIZATION [dbo];


GO
PRINT N'正在创建 表 [nilnul._acc].[Usr]...';


GO
CREATE TABLE [nilnul._acc].[Usr] (
    [id]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [name]     NVARCHAR (400) NULL,
    [pass_tip] NVARCHAR (400) NULL,
    [_time]    DATETIME       NULL,
    [_memo]    NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
PRINT N'正在创建 表 [nilnul._acc.usr].[Token]...';


GO
CREATE TABLE [nilnul._acc.usr].[Token] (
    [id]    BIGINT           IDENTITY (1, 1) NOT NULL,
    [usr]   BIGINT           NULL,
    [token] UNIQUEIDENTIFIER NULL,
    [_time] DATETIME         NULL,
    [_memo] NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
PRINT N'正在创建 默认约束 [nilnul._acc].[Usr] 上未命名的约束...';


GO
ALTER TABLE [nilnul._acc].[Usr]
    ADD DEFAULT getUtcDate() FOR [_time];


GO
PRINT N'正在创建 默认约束 [nilnul._acc.usr].[Token] 上未命名的约束...';


GO
ALTER TABLE [nilnul._acc.usr].[Token]
    ADD DEFAULT newid() FOR [token];


GO
PRINT N'正在创建 默认约束 [nilnul._acc.usr].[Token] 上未命名的约束...';


GO
ALTER TABLE [nilnul._acc.usr].[Token]
    ADD DEFAULT getUtcDate() FOR [_time];


GO
PRINT N'正在创建 外键 [nilnul._acc.usr].[Token] 上未命名的约束...';


GO
ALTER TABLE [nilnul._acc.usr].[Token] WITH NOCHECK
    ADD FOREIGN KEY ([usr]) REFERENCES [nilnul._acc].[Usr] ([id]);


GO
PRINT N'根据新创建的约束检查现有的数据';


GO
USE [$(DatabaseName)];


GO
CREATE TABLE [#__checkStatus] (
    id           INT            IDENTITY (1, 1) PRIMARY KEY CLUSTERED,
    [Schema]     NVARCHAR (256),
    [Table]      NVARCHAR (256),
    [Constraint] NVARCHAR (256)
);

SET NOCOUNT ON;

DECLARE tableconstraintnames CURSOR LOCAL FORWARD_ONLY
    FOR SELECT SCHEMA_NAME([schema_id]),
               OBJECT_NAME([parent_object_id]),
               [name],
               0
        FROM   [sys].[objects]
        WHERE  [parent_object_id] IN (OBJECT_ID(N'nilnul._acc.usr.Token'))
               AND [type] IN (N'F', N'C')
                   AND [object_id] IN (SELECT [object_id]
                                       FROM   [sys].[check_constraints]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0
                                       UNION
                                       SELECT [object_id]
                                       FROM   [sys].[foreign_keys]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0);

DECLARE @schemaname AS NVARCHAR (256);

DECLARE @tablename AS NVARCHAR (256);

DECLARE @checkname AS NVARCHAR (256);

DECLARE @is_not_trusted AS INT;

DECLARE @statement AS NVARCHAR (1024);

BEGIN TRY
    OPEN tableconstraintnames;
    FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
    WHILE @@fetch_status = 0
        BEGIN
            PRINT N'检查约束:' + @checkname + N' [' + @schemaname + N'].[' + @tablename + N']';
            SET @statement = N'ALTER TABLE [' + @schemaname + N'].[' + @tablename + N'] WITH ' + CASE @is_not_trusted WHEN 0 THEN N'CHECK' ELSE N'NOCHECK' END + N' CHECK CONSTRAINT [' + @checkname + N']';
            BEGIN TRY
                EXECUTE [sp_executesql] @statement;
            END TRY
            BEGIN CATCH
                INSERT  [#__checkStatus] ([Schema], [Table], [Constraint])
                VALUES                  (@schemaname, @tablename, @checkname);
            END CATCH
            FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
        END
END TRY
BEGIN CATCH
    PRINT ERROR_MESSAGE();
END CATCH

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') >= 0
    CLOSE tableconstraintnames;

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') = -1
    DEALLOCATE tableconstraintnames;

SELECT N'约束验证失败:' + [Schema] + N'.' + [Table] + N',' + [Constraint]
FROM   [#__checkStatus];

IF @@ROWCOUNT > 0
    BEGIN
        DROP TABLE [#__checkStatus];
        RAISERROR (N'验证约束时出错', 16, 127);
    END

SET NOCOUNT OFF;

DROP TABLE [#__checkStatus];


GO
PRINT N'更新完成。';


GO
